package taller.estructuras;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Lista<T extends Comparable<T>> implements List<T> {

	/**
	 * Tamaño de la lista
	 */
	private int size;

	/**
	 * Referencia al primer nodo de la lista
	 */
	private Node first;

	/**
	 * Referencia al último nodo de la lista
	 */
	private Node last;

	/**
	 * Constructor de la lista
	 */
	public Lista() {
		first = null;
		last = null;
	}

	@Override
	public boolean add(T e) {

		Node<T> nuevo = new Node<T>(e);

		if(first==null){

			first = nuevo;
			last = nuevo;
			size++;
			return true;

		}else{

			last.next = nuevo;
			last = nuevo;
			size++;
			return true;
		}

	}

	@Override
	public void add(int index, T element) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		first = null;
		last = null;
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T get(int index) {

		if(index>size-1||index<0)throw new IndexOutOfBoundsException();

		Node<T> actual = first;

		while(index--!=0){
			actual = actual.next;
		}

		return actual.elem;
	}

	@Override
	public int indexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		return size==0;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator<T> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T remove(int index) {

		if(index<0||index>size-1)throw new IndexOutOfBoundsException();

		T elem = null;

		if(index == 0){
			elem = (T) first.elem;
			first = first.next;
			last = --size==0? null:last;
		}else{
			Node<T> actual = first;
			while(index--!=1){
				actual = actual.next;
			}
			elem = (T)actual.next.elem;
			actual.next = actual.next.next;
			size--;
		}

		return null;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T set(int index, T element) {
		
		Node<T> actual = first;
		
		while(index-->0){
			actual = actual.next;
		}
		actual.elem = element;
		
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		Node<T> actual = first;
		int i = 0;
		
		while(actual!=null){
			array[i++] = actual.elem;
			actual = actual.next;
		}
		return array;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	public String toString(){
		Node<T> actual = first;
		String resp = "";

		while(actual!=null){
			resp+=actual.toString();
			actual = actual.next;
		}

		return resp;
	}

	private class Node<T>{
		T elem;
		Node next;

		public Node(T elem) {
			this.elem = elem;
			next = null;
		}
	}

}
