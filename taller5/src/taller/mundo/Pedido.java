﻿package taller.mundo;

public class Pedido implements Comparable<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	/**
	 * Tipo de pedido
	 */
	boolean despacho;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 */
	public Pedido(double pPrecio, String pAutorPedido, int pCercania) {
		precio = pPrecio;
		autorPedido = pAutorPedido;
		cercania = pCercania;
	}
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}
	
	@Override
	public int compareTo(Pedido comp) {
		
		int resp;
		
		if(!despacho)resp = precio>comp.getPrecio()? 1 : precio<comp.getPrecio()? -1 : 0;
		else resp = cercania<comp.getCercania()? 1 : cercania>comp.getCercania()? -1 : 0;
		
		return resp;
	}
	
}
