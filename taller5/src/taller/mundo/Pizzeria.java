﻿package taller.mundo;

import taller.estructuras.Heap;

public class Pizzeria 
{	
	// ----------------------------------
	// Constantes
	// ----------------------------------

	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Heap que almacena los pedidos recibidos
	 */
	private Heap<Pedido> recibidos;

	/** 
	 * Heap de elementos por despachar
	 */
	private Heap<Pedido> despacho;

	// ----------------------------------
	// Constructor
	// ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		recibidos = new Heap<Pedido>(); 
		despacho = new Heap<Pedido>();
	}

	// ----------------------------------
	// Métodos
	// ----------------------------------

	/**
	 * Getter de pedidos recibidos
	 */
	public Heap<Pedido> getRecibidos(){
		return recibidos;
	}

	/**
	 * Getter de elementos por despachar
	 */
	private Heap<Pedido> getPorDespachar(){
		return despacho;
	}

	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		Pedido ped = new Pedido(precio, nombreAutor, cercania);
		ped.despacho = false;
		recibidos.add(ped);  
	}

	// Atender al pedido más importante de la cola

	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		Pedido desp = recibidos.poll();
		desp.despacho = true;
		despacho.add(desp);
		
		return desp;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		return despacho.poll();
	}

	/**
	 * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
	 * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
	 */
	public Pedido [] pedidosRecibidosList()
	{ 
		Pedido[] pedidos = new Pedido[recibidos.size()];
		int i = 0;
		for (Comparable iter : recibidos.values()) {
			pedidos[i++] = (Pedido)iter;
		}
		
		return pedidos;
	}

	/**
	 * Retorna la cola de prioridad de despachos como un arreglo. 
	 * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
	 */
	public Pedido [] colaDespachosList()
	{
		Pedido[] pedidos = new Pedido[despacho.size()];
		int i = 0;
		for (Comparable iter : despacho.values()) {
			pedidos[i++] = (Pedido)iter;
		}
		
		return pedidos;
	}
}
