Estimaci�n de complejidad para Heap

Agregar/Swim
-----------------------------------------------------------------------------------------
- Considerando la implementaci�n de una lista encadenada con referencia al �ltimo nodo, 
la acci�n de agregar es constante y el intercambio entre el primer y �ltimo elemento es
de orden n en el peor caso. Por otra parte, se tiene que para hacer el m�todo swim la 
complejidad es nLog(n), debido a que es necesario recorrer la lista para analizar cada 
uno de los padres y el ascenso por niveles en un �rbol es logar�tmico.

Retornar M�ximo/M�nimo
-----------------------------------------------------------------------------------------
- Sabiendo que el Heap est� heap-ordenado, la verificaci�n del primer elemento es constante,
pues solo basta con consultar el primer elemento de la lista.

Retirar/Sink
-----------------------------------------------------------------------------------------
- Teniendo en cuenta que el mayor/menor elemento del heap se encuentra en el tope, remover
el elemento tiene una complejidad constante y el intercambio entre el primer y �ltimo elemento
es de orden n. Por otra parte, al igual que en el swim, se tiene que hacer un recorrido de 
la lista y un descenso en los niveles del �rbol que resultan en un orden linear�tmico nLog(n).

 